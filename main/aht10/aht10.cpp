#include "aht10/aht10.hpp"
#include <freertos/task.h>

namespace esp_idf
{

AHT10::AHT10(i2c_port_t port,
             TickType_t i2c_ticks_to_wait):
    i2c_port_(port),
    i2c_ticks_to_wait_(i2c_ticks_to_wait)
{
}

esp_err_t AHT10::initialize()
{
    i2c_cmd_handle_t cmd(i2c_cmd_link_create());
    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, i2c_addr_ << 1 | I2C_MASTER_WRITE, true);
    i2c_master_write_byte(cmd, soft_reset_cmd_, true);
    i2c_master_stop(cmd);
    esp_err_t rval(i2c_master_cmd_begin(i2c_port_, cmd, i2c_ticks_to_wait_));
    i2c_cmd_link_delete(cmd);
    if (rval != ESP_OK) {
        return rval;
    }

    // Wait for sensor ready
    vTaskDelay(pdMS_TO_TICKS(20));

    cmd = i2c_cmd_link_create();
    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, i2c_addr_ << 1 | I2C_MASTER_WRITE, true);
    uint8_t msg[3] { initialize_cmd_, enable_calibration_cmd_, nop_cmd_}; // Initialize, enable calibration, NOP
    i2c_master_write(cmd, msg, sizeof(msg), true);
    i2c_master_stop(cmd);
    rval = i2c_master_cmd_begin(i2c_port_, cmd, i2c_ticks_to_wait_);
    i2c_cmd_link_delete(cmd);
    if (rval != ESP_OK) {
        return rval;
    }

    // Wait for sensor ready
    vTaskDelay(pdMS_TO_TICKS(50));
    return rval;
}

esp_err_t AHT10::measureHumidityTemperature(float &humidity_perctg_relative,
        float &temperature_celcius)
{
    i2c_cmd_handle_t cmd(i2c_cmd_link_create());
    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, i2c_addr_ << 1 | I2C_MASTER_WRITE, true);
    uint8_t msg[3] { trigger_measurement_cmd, data_measurement_cmd_, nop_cmd_ }; // Trigger measurement
    i2c_master_write(cmd, msg, sizeof(msg), true);
    i2c_master_stop(cmd);
    esp_err_t rval(i2c_master_cmd_begin(i2c_port_, cmd, i2c_ticks_to_wait_));
    i2c_cmd_link_delete(cmd);
    if (rval != ESP_OK) {
        return rval;
    }

    // Wait for measurement to be ready
    vTaskDelay(pdMS_TO_TICKS(80));

    uint8_t data[6];
    unsigned error_count(0);
    while (1) {
        cmd = i2c_cmd_link_create();
        i2c_master_start(cmd);
        i2c_master_write_byte(cmd, i2c_addr_ << 1 | I2C_MASTER_READ, true);
        i2c_master_read(cmd, data, sizeof(data), I2C_MASTER_ACK);
        i2c_master_stop(cmd);
        rval = i2c_master_cmd_begin(i2c_port_, cmd, i2c_ticks_to_wait_);
        i2c_cmd_link_delete(cmd);

        calibrated_ = data[status_byte_] & calibrated_bit_mask_;
        working_mode_ = (data[status_byte_] >> 5) & 0x3;
        busy_ = data[status_byte_ ] & status_bit_mask_;

        if (!busy_) {
            break;
        }

        if (++error_count > 10) {
            return ESP_ERR_TIMEOUT;
        }

        vTaskDelay(pdMS_TO_TICKS(10));
    }

    const uint32_t raw_humidity = ((data[1] << 16) | (data[2] << 8) | data[3]) >> 4;
    humidity_perctg_relative = (float) raw_humidity * 100.0f / 1048576.0f;

    const uint32_t raw_temperature = ((data[3] & 0x0F) << 16) | (data[4] << 8) | data[5];
    temperature_celcius = ((float)raw_temperature * 200.0f / 0x100000) - 50.0f;
    return ESP_OK;
}

}

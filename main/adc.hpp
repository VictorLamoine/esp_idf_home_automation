#ifndef ADC_HPP
#define ADC_HPP

#include <driver/adc.h>


#ifdef HAS_PHOTO_RESISTOR
static const adc1_channel_t photo_resistor_channel(ADC1_CHANNEL_6); // GPIO34
static const adc_atten_t photo_resistor_adc_atten(ADC_ATTEN_DB_11);
static uint16_t read_photo_resistor(const unsigned samples = 32)
{
    uint32_t adc_reading(0);
    for (unsigned i(0); i < samples; ++i) {
        adc_reading += adc1_get_raw(photo_resistor_channel);
    }
    adc_reading /= samples;

    // ADC is 12 bits so max value is 4095
    return adc_reading;
}
#endif

#ifdef HAS_BATTERY_MONITORING
#include <esp_adc_cal.h>
#define DEFAULT_VREF 1100
esp_adc_cal_characteristics_t *adc_chars;
static const adc1_channel_t battery_monitoring_channel(ADC1_CHANNEL_5); // GPIO33
static const adc_atten_t battery_monitoring_adc_atten(ADC_ATTEN_DB_11);

static uint16_t read_battery_percentage(const unsigned samples = 32)
{
    uint32_t adc_reading(0);
    for (unsigned i(0); i < samples; ++i) {
        adc_reading += adc1_get_raw(battery_monitoring_channel);
    }
    adc_reading /= samples;

    // 1 NiMH - Charged: 1.4V, nominal: 1.2V, discharged: 0.9V
    // We measure 2 NiMH in series
    uint32_t voltage(esp_adc_cal_raw_to_voltage(adc_reading, adc_chars)); // milli Volts

    if (voltage >= 3000) {
        return 100;
    }
    if (voltage <= 1800) {
        return 0;
    }

    const float a(0.1);
    const float b(-180);
    float percentage(a * (float) voltage + b);
    return percentage;
}
#endif

static const adc_unit_t adc_unit(ADC_UNIT_1);
static void init_adc()
{
    adc1_config_width(ADC_WIDTH_BIT_12);
#ifdef HAS_PHOTO_RESISTOR
    adc1_config_channel_atten(photo_resistor_channel, photo_resistor_adc_atten);
#endif
#ifdef HAS_BATTERY_MONITORING
    adc1_config_channel_atten(battery_monitoring_channel, battery_monitoring_adc_atten);
    adc_chars = (esp_adc_cal_characteristics_t *) calloc(1, sizeof(esp_adc_cal_characteristics_t));
    esp_adc_cal_characterize(ADC_UNIT_1, battery_monitoring_adc_atten, ADC_WIDTH_BIT_12, DEFAULT_VREF, adc_chars);
#endif
}

#endif

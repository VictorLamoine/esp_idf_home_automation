#include "home_assistant/sensor.hpp"

namespace esp_idf
{
namespace home_assistant
{

Sensor::Sensor(const esp_mqtt_client_handle_t mqtt_client,
               const Sensor::DEVICE_CLASS device_class,
               const std::string state_class,
               const std::string name)
{
    client_ = mqtt_client;
    name_ = name;
    class_ = "sensor";
    state_class_ = state_class;
    if (device_class == DEVICE_CLASS::TEMPERATURE) {
        device_class_ = "temperature";
        unit_of_measurement_ = "°C"; // If you want stupid units like fahrenheit edit this
    } else if (device_class == DEVICE_CLASS::HUMIDITY) {
        device_class_ = "humidity";
        unit_of_measurement_ = "%";
    } else if (device_class == DEVICE_CLASS::BATTERY) {
        device_class_ = "battery";
        unit_of_measurement_ = "%";
    }

    tag_ = class_ + "/" + device_class_ + "/" + name_;
}

esp_err_t Sensor::publish(const float value,
                          const unsigned int precision,
                          const unsigned int qos,
                          const bool retain)
{
    if (!client_) {
        ESP_LOGE(tag_.c_str(), "MQTT client is not initialized");
        return ESP_FAIL;
    }

    std::string message(to_string_with_precision(value, precision));
    std::string topic(getMqttTopic() + "state");
    const int msg_id = esp_mqtt_client_publish(client_,
                       topic.c_str(),
                       message.c_str(),
                       message.size(),
                       qos,
                       retain);
    if (msg_id < 0) {
        ESP_LOGE(tag_.c_str(), "Could not publish MQTT message");
        return ESP_FAIL;
    }

    logging(topic, message, qos, retain);
    return ESP_OK;
}

std::string Sensor::to_string_with_precision(const float value,
        const unsigned int precision)
{
    std::string out(std::to_string(value));
    const std::size_t pos(out.find('.'));
    if (pos == std::string::npos) {
        return out;
    }
    if (precision == 0) {
        out.resize(pos);
    } else {
        out.resize(pos + 1 + precision);
    }

    return out;
}

}
}

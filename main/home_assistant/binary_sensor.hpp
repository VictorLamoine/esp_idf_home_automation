#ifndef HOME_ASSISTANT_BINARY_SENSOR
#define HOME_ASSISTANT_BINARY_SENSOR

#include "home_assistant/abstract_sensor.hpp"

namespace esp_idf
{
namespace home_assistant
{

class BinarySensor: public AbstractSensor
{
public:
    enum class DEVICE_CLASS {
        MOTION
    };

    BinarySensor(const esp_mqtt_client_handle_t mqtt_client,
                 const DEVICE_CLASS device_class,
                 const std::string name);

    esp_err_t publish(const bool status,
                      const unsigned qos = 0,
                      const bool retain = false);

};

}
}

#endif

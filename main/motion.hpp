#ifndef MOTION_HPP
#define MOTION_HPP

#include <driver/gpio.h>
#include <esp_log.h>
#include <freertos/queue.h>
#include "home_assistant/binary_sensor.hpp"

#define MOTION_GPIO GPIO_NUM_4
QueueHandle_t motion_queue;

static void IRAM_ATTR gpio_isr_handler(void *arg)
{
    gpio_num_t gpio((gpio_num_t)(uint32_t) arg);
    const bool state(gpio_get_level(gpio));
    xQueueSendFromISR(motion_queue, &state, NULL);
}

static void motion_sensor_init()
{
    gpio_config_t gpio_conf;
    gpio_conf.pin_bit_mask = 1ULL << MOTION_GPIO;
    gpio_conf.intr_type = GPIO_INTR_ANYEDGE;
    gpio_conf.mode = GPIO_MODE_INPUT;
    // Disable all because a 20 kohm resistor is already on
    // the AM312 board I bought on Aliexpress
    gpio_conf.pull_down_en = GPIO_PULLDOWN_DISABLE;
    gpio_conf.pull_up_en = GPIO_PULLUP_DISABLE;
    gpio_config(&gpio_conf);
    gpio_install_isr_service(ESP_INTR_FLAG_EDGE);
    gpio_isr_handler_add(MOTION_GPIO, gpio_isr_handler, (void *) MOTION_GPIO);
}

#endif

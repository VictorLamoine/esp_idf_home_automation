#include "kconfig.hpp"
#include "mqtt_tcp.hpp"
#include "wifi_station.hpp"

#include <esp_pm.h>
#include <driver/gpio.h>
#include <freertos/FreeRTOS.h>
#include <freertos/semphr.h>
#include <nvs_flash.h>

#ifdef HAS_MOTION_SENSOR
#include "motion.hpp"
#endif

#ifdef HAS_AHT10
#include "aht10.hpp"
#endif

#if defined HAS_PHOTO_RESISTOR || defined HAS_BATTERY_MONITORING
#include "adc.hpp"
#endif

#define BLINK_GPIO GPIO_NUM_2
static QueueHandle_t blink_queue;
esp_mqtt_client_handle_t mqtt_client(nullptr);
SemaphoreHandle_t mqtt_mutex(nullptr);

static void task_led_blink(void *)
{
    esp_pm_lock_handle_t light_sleep_lock;
    esp_pm_lock_create(ESP_PM_NO_LIGHT_SLEEP, 0, "blink", &light_sleep_lock);
    uint32_t blink_dur;
    while (1) {
        if (pdFAIL == xQueueReceive(blink_queue, &blink_dur, portMAX_DELAY)) {
            continue;
        }

        esp_pm_lock_acquire(light_sleep_lock);
        gpio_set_level(BLINK_GPIO, true);
        vTaskDelay(pdMS_TO_TICKS(blink_dur));
        gpio_set_level(BLINK_GPIO, false);
        vTaskDelay(pdMS_TO_TICKS(blink_dur));
        esp_pm_lock_release(light_sleep_lock);
    }
}

#ifdef HAS_MOTION_SENSOR
static void task_mqtt_motion(void *)
{
    uint32_t blink_dur(500);
    using esp_idf::home_assistant::BinarySensor;
    BinarySensor mqtt(mqtt_client,
                      BinarySensor::DEVICE_CLASS::MOTION,
                      CONFIG_ROOM_PLACE);
    ESP_ERROR_CHECK(mqtt.advertise());
    esp_log_level_set(mqtt.tag(), ESP_LOG_INFO);

    bool state;
    esp_err_t rval;
    while (1) {
        if (pdFAIL == xQueueReceive(motion_queue, &state, portMAX_DELAY)) {
            continue;
        }
        ESP_LOGD("MQTT_MOTION", "%s edge", (state ? "Rising" : "Falling"));
        xSemaphoreTake(mqtt_mutex, portMAX_DELAY);
        rval = mqtt.publish(state);
        xSemaphoreGive(mqtt_mutex);
        if (rval != ESP_OK) {
            ESP_LOGE("MQTT_MOTION", "Could not send motion MQTT message: %s", esp_err_to_name(rval));
        } else {
            xQueueSendToBack(blink_queue, &blink_dur, pdMS_TO_TICKS(5));
        }
    }
}
#endif

#ifdef HAS_AHT10
static void task_aht10(void *)
{
    esp_idf::AHT10 aht10(I2C_PORT);
    ESP_ERROR_CHECK(aht10.initialize());
    esp_err_t rval;
    float humidity, temperature;

    while (1) {
        rval = aht10.measureHumidityTemperature(humidity, temperature);
        if (rval != ESP_OK) {
            ESP_LOGE("AHT10", "Could not fetch temperature and humidity: %s", esp_err_to_name(rval));
            vTaskDelay(pdMS_TO_TICKS(5000));
        }

        ESP_LOGI("AHT10", "Temperature = %f°C, Humidity = %f prct", temperature, humidity);

        if (pdTRUE != xQueueSendToBack(humidity_queue, &humidity, pdMS_TO_TICKS(5))) {
            ESP_LOGE("AHT10", "Humidity queue is full");
        } else {
            ESP_LOGD("AHT10", "Queued humidity");
        }

        if (pdTRUE != xQueueSendToBack(temperature_queue, &temperature, pdMS_TO_TICKS(5))) {
            ESP_LOGE("AHT10", "Temperature queue is full");
        } else {
            ESP_LOGD("AHT10", "Queued temperature");
        }

        vTaskDelay(pdMS_TO_TICKS(CONFIG_SENSORS_PERIOD * 1000));
    }
}

static void task_mqtt_humidity(void *)
{
    uint32_t blink_dur(250);
    using esp_idf::home_assistant::Sensor;
    Sensor mqtt(mqtt_client,
                Sensor::DEVICE_CLASS::HUMIDITY,
                "measurement", // Allows for long term storage
                CONFIG_ROOM_PLACE);
    ESP_ERROR_CHECK(mqtt.advertise());
    esp_log_level_set(mqtt.tag(), ESP_LOG_INFO);

    float humidity;
    esp_err_t rval;
    while (1) {
        if (pdFAIL == xQueueReceive(humidity_queue, &humidity, portMAX_DELAY)) {
            continue;
        }

        xSemaphoreTake(mqtt_mutex, portMAX_DELAY);
        rval = mqtt.publish(humidity);
        xSemaphoreGive(mqtt_mutex);
        if (rval != ESP_OK) {
            ESP_LOGE("MQTT_HUMIDITY", "Could not send humidity MQTT message: %s", esp_err_to_name(rval));
        } else {
            xQueueSendToBack(blink_queue, &blink_dur, pdMS_TO_TICKS(5));
        }
    }
}

static void task_mqtt_temperature(void *)
{
    uint32_t blink_dur(250);
    using esp_idf::home_assistant::Sensor;
    Sensor mqtt(mqtt_client,
                Sensor::DEVICE_CLASS::TEMPERATURE,
                "measurement", // Allows for long term storage
                CONFIG_ROOM_PLACE);
    ESP_ERROR_CHECK(mqtt.advertise());
    esp_log_level_set(mqtt.tag(), ESP_LOG_INFO);

    float temperature;
    esp_err_t rval;
    while (1) {
        if (pdFAIL == xQueueReceive(temperature_queue, &temperature, portMAX_DELAY)) {
            continue;
        }

        xSemaphoreTake(mqtt_mutex, portMAX_DELAY);
        rval = mqtt.publish(temperature);
        xSemaphoreGive(mqtt_mutex);
        if (rval != ESP_OK) {
            ESP_LOGE("MQTT_TEMPERATURE", "Could not send temperature MQTT message: %s", esp_err_to_name(rval));
        } else {
            xQueueSendToBack(blink_queue, &blink_dur, pdMS_TO_TICKS(5));
        }
    }
}
#endif


#ifdef HAS_PHOTO_RESISTOR
static void task_photo_resistor(void *)
{
    esp_log_level_set("PHOTORESISTOR", ESP_LOG_INFO);
    uint16_t raw;
    while (1) {
        raw = read_photo_resistor();
        ESP_LOGI("PHOTORESISTOR", "Raw value: %d", raw);
        vTaskDelay(pdMS_TO_TICKS(5000));
    }
}
#endif

#ifdef HAS_BATTERY_MONITORING
void task_mqtt_battery(void *)
{
    uint32_t blink_dur(100);
    using esp_idf::home_assistant::Sensor;
    Sensor mqtt(mqtt_client,
                Sensor::DEVICE_CLASS::BATTERY,
                "", // No long term storage
                CONFIG_ROOM_PLACE);
    ESP_ERROR_CHECK(mqtt.advertise());
    esp_log_level_set(mqtt.tag(), ESP_LOG_INFO);

    esp_pm_lock_handle_t light_sleep_lock;
    esp_pm_lock_create(ESP_PM_NO_LIGHT_SLEEP, 0, "blink", &light_sleep_lock);

    uint16_t percentage;
    esp_err_t rval;
    while (1) {
        xSemaphoreTake(mqtt_mutex, portMAX_DELAY);
        esp_pm_lock_acquire(light_sleep_lock);
        // Wait 1 second after last MQTT communication
        vTaskDelay(pdMS_TO_TICKS(1000));
        // When sending MQTT message the battery voltage drops and gives wrong reading
        percentage = read_battery_percentage();
        rval = mqtt.publish(percentage, 0);
        xSemaphoreGive(mqtt_mutex);
        esp_pm_lock_release(light_sleep_lock);

        if (rval != ESP_OK) {
            ESP_LOGE("MQTT_BATTERY", "Could not send battery MQTT message: %s", esp_err_to_name(rval));
        } else {
            xQueueSendToBack(blink_queue, &blink_dur, pdMS_TO_TICKS(5));
        }

        vTaskDelay(pdMS_TO_TICKS(CONFIG_SENSORS_PERIOD * 1000));
    }
}
#endif

extern "C" void app_main()
{
    esp_log_level_set("*", ESP_LOG_WARN);
    esp_log_level_set("MQTT", ESP_LOG_INFO);

    // Init LED GPIO
    esp_rom_gpio_pad_select_gpio(BLINK_GPIO);
    gpio_set_direction(BLINK_GPIO, GPIO_MODE_OUTPUT);

    // NVS memory is required for Wi-Fi
    esp_err_t rval(nvs_flash_init());
    if (rval == ESP_ERR_NVS_NO_FREE_PAGES || rval == ESP_ERR_NVS_NEW_VERSION_FOUND) {
        ESP_ERROR_CHECK(nvs_flash_erase());
        rval = nvs_flash_init();
    }
    ESP_ERROR_CHECK(rval);

    // Dynamic frequency scaling and automatic light sleep
    esp_pm_config_t pm_config = {
        .max_freq_mhz = 240,
        .min_freq_mhz = 80,
#if CONFIG_FREERTOS_USE_TICKLESS_IDLE
        .light_sleep_enable = true
#else
        .light_sleep_enable = false
#endif
    };
    ESP_ERROR_CHECK(esp_pm_configure(&pm_config));

    wifi_init_sta();

#ifdef HAS_MOTION_SENSOR
    // Queue is created before MQTT init because MQTT subscriber on alarm
    // can push in the motion queue (with state = off)
    motion_queue = xQueueCreate(10, sizeof(bool));
#endif
    mqtt_init(mqtt_client);
    mqtt_mutex = xSemaphoreCreateBinary();
    xSemaphoreGive(mqtt_mutex);

    blink_queue = xQueueCreate(10, sizeof(uint32_t));
    xTaskCreate(task_led_blink, "led_blink", 4096, NULL, configMAX_PRIORITIES - 2, NULL);

#ifdef HAS_AHT10
    // I2C is used with the AHT10 sensor
    ESP_ERROR_CHECK(i2c_master_init());
    temperature_queue = xQueueCreate(10, sizeof(float));
    humidity_queue = xQueueCreate(10, sizeof(float));
    xTaskCreate(task_aht10, "aht10", 4096, NULL, configMAX_PRIORITIES - 1, NULL);
    xTaskCreate(task_mqtt_humidity, "mqtt_humidity", 4096, NULL, configMAX_PRIORITIES - 1, NULL);
    xTaskCreate(task_mqtt_temperature, "mqtt_temperature", 4096, NULL, configMAX_PRIORITIES - 1, NULL);
#endif

#ifdef HAS_MOTION_SENSOR
    motion_sensor_init();
    xTaskCreate(task_mqtt_motion, "mqtt_motion", 4096, NULL, configMAX_PRIORITIES - 1, NULL);
#endif

#if defined HAS_PHOTO_RESISTOR || defined HAS_BATTERY_MONITORING
    init_adc();
#endif

#ifdef HAS_PHOTO_RESISTOR
    xTaskCreate(task_photo_resistor, "photo_resistor", 4096, NULL, configMAX_PRIORITIES - 1, NULL);
#endif

#ifdef HAS_BATTERY_MONITORING
    xTaskCreate(task_mqtt_battery, "mqtt_battery", 4096, NULL, configMAX_PRIORITIES - 1, NULL);
#endif
}

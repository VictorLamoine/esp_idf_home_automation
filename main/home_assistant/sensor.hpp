#ifndef HOME_ASSISTANT_SENSOR_HPP
#define HOME_ASSISTANT_SENSOR_HPP

#include "home_assistant/abstract_sensor.hpp"

namespace esp_idf
{
namespace home_assistant
{

class Sensor: public AbstractSensor
{
public:
    enum class DEVICE_CLASS {
        TEMPERATURE,
        HUMIDITY,
        BATTERY
    };

    Sensor(const esp_mqtt_client_handle_t mqtt_client,
           const DEVICE_CLASS device_class,
           const std::string state_class,
           const std::string name);

    esp_err_t publish(const float value,
                      const unsigned precision = 1,
                      const unsigned qos = 0,
                      const bool retain = false);

private:
    std::string to_string_with_precision(const float value,
                                         const unsigned precision = 1);
};

}
}

#endif


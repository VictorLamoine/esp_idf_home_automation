#ifndef MQTT_TCP_HPP
#define MQTT_TCP_HPP

#include <esp_event.h>
#include <esp_log.h>
#include <freertos/event_groups.h>
#include <freertos/FreeRTOS.h>
#include <freertos/queue.h>
#include <freertos/task.h>
#include <mqtt_client.h>
#include "sdkconfig.h"
#include <string>

#ifdef CONFIG_HAS_MOTION_SENSOR
#include "motion.hpp"
#endif

static EventGroupHandle_t mqtt_event_group;
const static int CONNECTED_BIT = BIT0;

static void mqtt_event_handler(void *handler_args, esp_event_base_t base, int32_t event_id, void *event_data)
{
    esp_mqtt_event_handle_t event = (esp_mqtt_event_handle_t) event_data;

    esp_mqtt_client_handle_t client(event->client);
    switch (event->event_id) {
    case MQTT_EVENT_CONNECTED: {
        ESP_LOGI("MQTT", "MQTT_EVENT_CONNECTED");
        xEventGroupSetBits(mqtt_event_group, CONNECTED_BIT);
        const int msg_id(esp_mqtt_client_subscribe(client, CONFIG_HOME_ASSISTANT_ALARM_TOPIC, 0));
        ESP_LOGI("MQTT", "Subscribed to %s, msg_id = %d", CONFIG_HOME_ASSISTANT_ALARM_TOPIC, msg_id);
        break;
    }
    case MQTT_EVENT_DATA: {
        std::string topic;
        topic.insert(0, event->topic, event->topic_len);
        std::string data;
        data.insert(0, event->data, event->data_len);

        if (topic == CONFIG_HOME_ASSISTANT_ALARM_TOPIC) {
            if (data == "on") {
                ESP_LOGI("MQTT", "Alarm sound enabled");
                // FIXME Not used
            } else if (data == "off") {
                ESP_LOGI("MQTT", "Alarm sound disabled");
                // FIXME Not used
            }
        }
        break;
    }
    default: {
        ESP_LOGI("MQTT", "Other event id: %d", event->event_id);
        break;
    }
    }
}

static void mqtt_init(esp_mqtt_client_handle_t &mqtt_client)
{
    esp_mqtt_client_config_t mqtt_cfg{};
    mqtt_cfg.broker.address.uri = CONFIG_BROKER_TCP_URI;
    mqtt_client = esp_mqtt_client_init(&mqtt_cfg);

    esp_mqtt_client_register_event(mqtt_client, MQTT_EVENT_ANY, mqtt_event_handler, NULL);
    mqtt_event_group = xEventGroupCreate();
    xEventGroupClearBits(mqtt_event_group, CONNECTED_BIT);
    ESP_ERROR_CHECK(esp_mqtt_client_start(mqtt_client));
    xEventGroupWaitBits(mqtt_event_group, CONNECTED_BIT, false, true, portMAX_DELAY);
}

#endif

#ifndef ATH10_HPP
#define ATH10_HPP

#include <driver/i2c.h>

namespace esp_idf
{

class AHT10
{
public:
    AHT10(i2c_port_t port,
          TickType_t i2c_ticks_to_wait = 1000 / portTICK_PERIOD_MS);

    esp_err_t initialize();
    esp_err_t measureHumidityTemperature(float &humidity_perctg_relative,
                                         float &temperature_celcius);
    bool calibrated_ = false;
    int working_mode_ = 0;
    bool busy_ = true;

private:
    const i2c_port_t i2c_port_;
    const TickType_t i2c_ticks_to_wait_;

    const uint8_t i2c_addr_ = 0x38;
    const uint8_t soft_reset_cmd_ = 0xBA;
    const uint8_t initialize_cmd_ = 0xE1;
    const uint8_t enable_calibration_cmd_ = 0x08;
    const uint8_t trigger_measurement_cmd = 0xAC;
    const uint8_t data_measurement_cmd_ = 0x33; // No datasheet documentation...
    const uint8_t nop_cmd_ = 0x0; // No operation

    const uint8_t status_byte_ = 0;
    const uint8_t calibrated_bit_mask_ = 0x8;
    const uint8_t status_bit_mask_ = 0x80;
};

}

#endif

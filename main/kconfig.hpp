#ifndef KCONFIG_HPP
#define KCONFIG_HPP

#include "sdkconfig.h"

#if CONFIG_HAS_AHT10 == 1
#define HAS_AHT10
#endif

#if CONFIG_HAS_MOTION_SENSOR == 1
#define HAS_MOTION_SENSOR
#endif

#if CONFIG_HAS_PHOTO_RESISTOR == 1
#define HAS_PHOTO_RESISTOR
#endif

#if CONFIG_HAS_BATTERY_MONITORING == 1
#define HAS_BATTERY_MONITORING
#endif

#endif

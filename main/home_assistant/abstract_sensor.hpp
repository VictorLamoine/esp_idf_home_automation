#ifndef HOME_ASSISTANT_ABSTRACT_SENSOR_HPP
#define HOME_ASSISTANT_ABSTRACT_SENSOR_HPP

#include <string>
#include <esp_err.h>
#include <esp_log.h>
#include <mqtt_client.h>

namespace esp_idf
{
namespace home_assistant
{

class AbstractSensor
{
public:
    virtual ~AbstractSensor() = 0;
    esp_err_t advertise(const unsigned qos = 0,
                        const bool retain = true);
    esp_err_t deadvertise(const unsigned qos = 0,
                          const bool retain = true);

    const char *tag() const;

protected:
    std::string getMqttTopic();

    void logging(const std::string &topic,
                 const std::string &message,
                 const unsigned &qos,
                 const bool &retain);

    std::string tag_;
    const std::string homeassistant_prefix_ = "homeassistant/";
    esp_mqtt_client_handle_t client_;
    std::string name_;
    std::string class_;
    std::string device_class_;
    std::string state_class_;
    std::string unit_of_measurement_;
};

}
}

#endif

#include "home_assistant/abstract_sensor.hpp"

namespace esp_idf
{
namespace home_assistant
{

AbstractSensor::~AbstractSensor()
{
}

esp_err_t AbstractSensor::advertise(const unsigned int qos,
                                    const bool retain)
{
    if (!client_) {
        return ESP_FAIL;
    }

    std::string message("{");
    message.append("\"unique_id\": \"" + name_ + "_" + device_class_ + "\"");
    message.append(", \"name\": \"" + name_ + "_" + device_class_ + "\"");
    if (!state_class_.empty()) {
        message.append(", \"state_class\": \"" + state_class_ + "\"");
    }
    message.append(", \"device_class\": \"" + device_class_ + "\"");
    if (!unit_of_measurement_.empty()) {
        message.append(", \"unit_of_measurement\": \"" + unit_of_measurement_ + "\"");
    }
    message.append(", \"state_topic\": \"" + getMqttTopic() + "state\"");
    message.append("}");

    std::string topic(getMqttTopic() + "config");
    const int msg_id = esp_mqtt_client_publish(client_,
                       topic.c_str(),
                       message.c_str(),
                       message.size(),
                       qos,
                       retain);
    if (msg_id < 0) {
        ESP_LOGE(tag_.c_str(), "Could not publish MQTT message");
        return ESP_FAIL;
    }

    logging(topic, message, qos, retain);
    return ESP_OK;
}

esp_err_t AbstractSensor::deadvertise(const unsigned qos,
                                      const bool retain)
{
    if (!client_) {
        return ESP_FAIL;
    }

    std::string message("");
    std::string topic(getMqttTopic() + "config");
    const int msg_id = esp_mqtt_client_publish(client_,
                       topic.c_str(),
                       message.c_str(),
                       message.size(),
                       qos,
                       retain);
    if (msg_id < 0) {
        ESP_LOGE(tag_.c_str(), "Could not publish MQTT message");
        return ESP_FAIL;
    }

    logging(topic, message, qos, retain);
    return ESP_OK;
}

const char *AbstractSensor::tag() const
{
    return tag_.c_str();
}

std::string AbstractSensor::getMqttTopic()
{
    return homeassistant_prefix_ + class_ + "/" + name_ + "/" + device_class_ + "/";
}

void AbstractSensor::logging(const std::string &topic,
                             const std::string &message,
                             const unsigned &qos,
                             const bool &retain)
{
    ESP_LOGI(tag_.c_str(),
             "Published MQTT message:\n"
             "\tTopic: %s\n"
             "\tMessage: %s\n"
             "\tQoS: %d\n"
             "\tretain: %s\n",
             topic.c_str(),
             message.c_str(),
             qos,
             retain ? "yes" : "no");
}

}
}

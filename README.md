This repository contains a home automation project using an ESP32 microcontroller and a PCB design.

Sensors:
- PIR motion sensor
- Humidity and temperature
- Photo resistor
- Battery voltage (only 2 NiMH batteries are checked of the 4 that powers the board)

Values are sent on MQTT topics (via Wi-Fi) and can be retrieved in [Home Assistant](https://www.home-assistant.io) (or other).

# C++ project
C++ code for the ESP32 to fetch data from sensors and publish them on MQTT topics

## Dependencies
[Get ESP-IDF](https://docs.espressif.com/projects/esp-idf/en/stable/esp32/get-started/index.html#step-2-get-esp-idf)

# Compiling
```bash
git clone https://gitlab.com/VictorLamoine/esp_idf_home_automation.git
cd esp_idf_home_automation
get_idf # See https://docs.espressif.com/projects/esp-idf/en/stable/esp32/get-started/index.html#id5
```

Use `idf.py menuconfig` to configure the project, these sub-menus must be visited:
- `Wi-Fi`
- `MQTT`
- `Sensors`

Power save configuration:
In `menuconfig`, `Component config`:
- ESP32 specific: CPU frequency: 80 MHz
- Power management: yes
- FreeRTOS
  - Run on first core: yes
  - Tickless idle support: yes

# Uploading and monitoring
```bash
idf.py flash monitor
```

# KiCad EDA
[KiCad EDA for the PCB](eda)

You need to install all the symbols and footprints in KiCad: https://gitlab.com/VictorLamoine/kicad

![KiCad 3D view](eda/3d_view.png)

# Mechanical design
[OnShape CAD](https://cad.onshape.com/documents/149521df3ab12ad456924627/w/5d87e7bcbd60f58c2ea5b51c/e/2b51c9865d167ef5a5358234)

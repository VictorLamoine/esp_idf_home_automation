#include "home_assistant/binary_sensor.hpp"

namespace esp_idf
{
namespace home_assistant
{

BinarySensor::BinarySensor(const esp_mqtt_client_handle_t mqtt_client,
                           const BinarySensor::DEVICE_CLASS device_class,
                           const std::string name)
{
    client_ = mqtt_client;
    name_ = name;
    class_ = "binary_sensor";
    if (device_class == DEVICE_CLASS::MOTION) {
        device_class_ = "motion";
    }
    unit_of_measurement_ = "";

    tag_ = class_ + "/" + device_class_ + "/" + name_;
}

esp_err_t BinarySensor::publish(const bool status,
                                const unsigned int qos,
                                const bool retain)
{
    if (!client_) {
        ESP_LOGE(tag_.c_str(), "MQTT client is not initialized");
        return ESP_FAIL;
    }

    std::string message(status ? "ON" : "OFF");
    std::string topic(getMqttTopic() + "state");
    const int msg_id = esp_mqtt_client_publish(client_,
                       topic.c_str(),
                       message.c_str(),
                       message.size(),
                       qos,
                       retain);
    if (msg_id < 0) {
        ESP_LOGE(tag_.c_str(), "Could not publish MQTT message");
        return ESP_FAIL;
    }

    logging(topic, message, qos, retain);
    return ESP_OK;
}

}
}

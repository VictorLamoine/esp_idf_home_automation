#ifndef AHT10_HPP
#define AHT10_HPP

#include "aht10/aht10.hpp"
#include <driver/i2c.h>
#include <freertos/queue.h>
#include "home_assistant/sensor.hpp"

#define I2C_PORT I2C_NUM_0
#define I2C_SDA GPIO_NUM_21 // Data
#define I2C_SCL GPIO_NUM_22 // Clock

QueueHandle_t temperature_queue;
QueueHandle_t humidity_queue;

static esp_err_t i2c_master_init()
{
    i2c_config_t conf {};
    conf.mode = I2C_MODE_MASTER;
    conf.sda_io_num = I2C_SDA;
    conf.sda_pullup_en = GPIO_PULLUP_ENABLE;
    conf.scl_io_num = I2C_SCL;
    conf.scl_pullup_en = GPIO_PULLUP_ENABLE;
    conf.master.clk_speed = 100000; // 100 kHz, max for AHT10
    i2c_param_config(I2C_PORT, &conf);
    return i2c_driver_install(I2C_PORT, conf.mode, 0, 0, 0);
}

#endif
